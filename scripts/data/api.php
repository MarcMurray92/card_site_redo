<?php
        // set up the connection variables
        $db_name  = 'card_site';
        $hostname = 'localhost';
        $username = 'root';
        $password = 'root';

        // connect to the database
        $dbh = new PDO("mysql:host=$hostname;dbname=$db_name", $username, $password);
        // Ask Jay bout better way for this ^^^

        // a query get all the records from the users table
        $sql = 'SELECT ID, card_title, card_category, card_rating, card_image FROM cards';

        // use prepared statements, even if not strictly required is good practice
        $stmt = $dbh->prepare( $sql );

        // execute the query
        $stmt->execute();

        // fetch the results into an array
        $result = $stmt->fetchAll( PDO::FETCH_ASSOC );

        // convert to json
        $json = json_encode( $result, JSON_NUMERIC_CHECK );

        // echo the json string
        echo $json;
?>