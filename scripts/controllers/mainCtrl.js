angular.module("cardSite", ['masonry', 'ngAnimate', 'ui.bootstrap'])
    .controller('mainCtrl', function($scope, dataService, $scope, $uibModal, $log) {
        $scope.cards = [];
        dataService.getCards().then(function(response) {
            $scope.cards = response.data;
        }, function() {});
        $scope.plusOne = function(card) {
            card.card_rating += 1;
        };
        $scope.isActive = false;
        $scope.changeClass = function() {
            $scope.isActive = !$scope.isActive;
        };
        // Modal stuff here
        $scope.modalOpen = function(size, selectedCard) {

            var modalInstance = $uibModal.open({
                animation: $scope.animationsEnabled,
                templateUrl: 'sendCard.html',
                controller: function($scope, $uibModalInstance, card) {

                    $scope.card = card;

                    $scope.send = function() {
                        $uibModalInstance.close($scope.selectedCard);
                        console.log($scope.selectedCard);
                    };

                    $scope.cancel = function() {
                        $uibModalInstance.dismiss('cancel');
                    };

                },
                size: size,
                resolve: {
                    card: function() {
                        return selectedCard;
                    }
                }
            });

            modalInstance.result.then(function(selectedCard) {
                $scope.selected = selectedCard;
            }, function() {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };


    });
