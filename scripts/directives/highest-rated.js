//todo.js
angular.module('cardSite')
 .directive('highestRated', function() {
   return {
    templateUrl: 'templates/highest-rated.html',
    controller: 'mainCtrl',
    replace: true //added this line
    };
});