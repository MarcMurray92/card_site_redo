//todo.js
angular.module('cardSite')
 .directive('christmasHomepage', function() {
   return {
    templateUrl: 'templates/christmas-homepage.html',
    controller: 'mainCtrl',
    replace: true //added this line
    };
});