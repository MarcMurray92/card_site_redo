angular.module("cardSite")
    .service('dataService', function($http) {
        // DB GET function
        this.getCards = function() {
            return $http.get("scripts/data/api.php")
        };
    });